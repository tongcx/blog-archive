---
layout: page
title: Resume
permalink: /resume/
---

I, Chaoxu Tong, am a PhD student at Cornell University, majoring Operations Research. I enjoy **applying, adapting, designing optimization and modeling techniques to solve problems with real-life complexity**.

Recently, I am working on using real-time information to improve efficiency in healthcare. Combining easy to use, beautiful frontend and intelligent backend, I try to help people and organization make better decisions using optimization and machine learning techniques.

I contributes to open source community, focusing on machine learning and optimization algorithms and application. Tools in opeartions research are extremely useful in real-life problem. So I'm **very passionate on making operations research knowledges accessible to programmers and building high-quality open source operations research tools**.

In my spare time, I like to play basketball, talk with people (both technically and non-technically), reading novels, participating programming contests, watching TV show and Japanese anime. By the way, please help me to pick up tennis again.

I speak Chinese, English and Spanish(coming soon).


# Education

- 2010-2015 **Cornell University**, PhD in Operations Research
- 2006-2010 **University of Science and Technology in China**, BS in Math


# Skills

Here is a list of tools I often/sometimes use

- Major use: Java, Python, R, Haskell, C++
- Minor use: Scala, Ocamel, Go, Matlab
- Web development: Node.js, Ruby, Python, Javascript, CSS, D3
- Database: MongoDB, SQL, Neo4j, Hadoop, Spark
- Optimization: CPLEX, Gurobi, AMPL
- Machine Learning: scikit-learn, SVM-Light


# Experience

- 2014/10 - current, Cornell University, real-time patient flow optimization
- 2014/1 - 2014/8, eBay, Data Scientist Intern, optimizing same-day local delivery
- 2013/8 - 2013/11, Waterloo University, multi-service facility location model
- 2012/6 - 2012/8, Facebook, Software Engineer Intern, optimizing ad reserve prices
- 2011/6 - 2011/8, Cornell University, bounds on ambulance deployment policy
- 2011/4 - 2011/6, Cornell University, fast airticket pricing and customer choice model

# Activities

- 2014 Google Codejam Online Round 2
- 2013 Google Codejam Online Round 3
- 2012 Ta-Chung & Ya-Chao Liu Memorial Award, Cornell University
- 2011 ACM/ICPC, Greater New York Regional, 4th place
- 2010 Hack U Web Development, Cornell University, 2nd place
- 2009 Mathematical Contest in Modeling, Meritorious Winner
- 2007 ACM/ICPC, Nanjing Regional Golden Prize
