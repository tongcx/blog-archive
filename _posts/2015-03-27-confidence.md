---
layout: post
title:  "Base your confidence on nothing"
tags:
- philosophy
---

Friends, where should you base your confidence?

- Confident if you are wealthy? There are plenty wealthy, but unconfident people.
- Confident if you are physically strong? It definitely helps, but I also saw people with serious illness with firm confidence.
- Confident if you have lots of achievement? Younger and smarter people will come to take the fame. Past is past.
- Confident if you are knowledgeable or you master lots of skills? Human beings still have lots of unsolved problems. Also, there are always people more knowledgeable, master more skills than you.

We all know we should build skyscraper on firm ground. But wealthy/strong body/achievement/knowledge/skills are at most illusionary foundation for confidence. Ironically, they bring you confidence with string and they will pull the string later, leaving you high and dry. They only rent, but not sell, confidence to you.

So where should you base your confidence?

When we are talking about wealth, strong body, etc., we are talking about facts. It seems these facts are bolstering our confidence. But consider the following fact: leaves are green. Should you be more confident or less confident with this fact? **The fact that you're wealthy, strong, etc. is no different from "leaves are green". Facts are neutral. It's our interpretation making us more or less happy.** Believe me, you can interpret "leaves are green" in a thousand ways to make you either happy or unhappy.

This leads to an interesting point. We may base our confidence in the ability to interpret fact in happy way. Well, since you can interpret in either way, why not choose the happy one. So what are some fundamental examples?

- Believe you are born with significance.
- Believe you are and will be enjoying life.
- Believe you can love and be loved by others.
- Believe you're confident.
- Believe in yourself.

The key is simply belief.

However, don't believe you're always doing the right nothing, we all do wrong things from time to from, both to others and ourself. But you can believe these happened because they're inevitable on the road to a better self. It's just like you must climb the mountain before you can see from the top.

Last but not least, I hope this will help you to free yourself and follow your hunch when, saying, playing basketball. Instead of do what expert says, you can just enjoy the moment. Life is like a beautiful journey, have fun.

PS: Lots of people cannot help to compare themselves with others, I will post something later on that topic.

PS: Open for discussion, please leave your comments.
