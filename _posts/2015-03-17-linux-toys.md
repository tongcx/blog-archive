---
layout: post
title:  "Some Linux Toys"
tags:
- linux
---
Yesterday I am trying to get Solarized color scheme to work with mac + ssh + bash + tmux + vim. When juggling with this, my interest switched to find better alternatives for the most fundamental softwares in Linux.

So as a programmer, what do you use most within Linux?

- For GUI, you probably use GNOME, KDE as so on. Recently, I found myself closing and opening the same window again and again, just because I cannot see already opened ones. I need some new way of managing windows.
- For terminal, you probably use xterm or gnome-terminal. They're usually fine to me as a user, unless you want to configure it without GUI.
- For shell, I would be mostly correct if I guess you're using bash/zsh/ksh. Whoa! But you know programming with bash is as pleasant as using [Brainfuck](http://en.wikipedia.org/wiki/Brainfuck).

So what did I do? I replace them all

- For Window Manager, I switch to [i3](https://i3wm.org/). This is a tiling windows manager with very easy configuration. Basically you won't waste any space on your screen and you can see all open windows.
- For Terminal, there is [termite](https://github.com/thestinger/termite). Very easy to customize.
- For shell, [fish](http://fishshell.com/) is tasty! Fantastic tab completion, history search and most importantly, a sane shell programming language!
- My Linux distribution is Arch Linux. Beautiful distribution. You can learn a lot reading ArchWiki. (In some sense you have to read it if you want to use this distribution)

For difference between shell and terminal, check out [this](http://superuser.com/a/144668)

The setup is not totally trivial, so I record what happens here for later reference.


# Setting up i3

Before i3, I was using GNOME, where most things are taken care for users and you can configure most things with GUI. It's like going wild of discarding GNOME. Some background knowledge Before going to more details.

## How GUI works in Linux

- Everything graphical relies on X Window System to provide the basic drawing and display functionality.
- When you computer at the last stage of [booting](https://wiki.archlinux.org/index.php/Arch_boot_process) (`graphical.target` if you're using systemd, `man 7 bootup` for some surprise), a Display Manager (GDM, KDM, SDDM, etc) is invoked. It basically shows some users and ask for passwords. Also they can detect installed Window Manger by looking at `/usr/share/xsessions`.
- Display manager gives control to Window Manager, who manages how windows layout.
- Most likely you're using a Desktop Environment (GNOME, KDE, etc) providing Display Manager, Windows Manager and a bunch of other softwares like GUI editor.

## Actually setting up i3

Well, initially I tried to still use some Display Manager. But it's a pain in the ass to make my dual monitor works. So I just get rid of Display Manager, changed the initial booting target by

    sudo systemctl set-default multi-user.target

where it will finish with a prompt. To tell X to use i3, change you xinitrc (startx will load this file)

    ~/.xinitrc
    ----------
    xrandr --output DVI-1 --auto --left-of DVI-0 # config dual monitor, this line must come first, otherwise both status bars appear on the same monitor
    exec i3                                      # start i3 window manager

Then directly start i3 by `startx`.

Mission completed! Not really, now you can remove blow out GNOME and check out [i3's tutorial](http://i3wm.org/docs/userguide.html) or [cheatsheet](http://i3wm.org/docs/refcard.html).

# Termite & fish

    # install fish and termite
    sudo pacman -S fish
    packer -S termite-git

    # remove terminals who have a higher prioirity over termite in i3-sensible-terminal
    # otherwise, you can set $TERMINAL to tell i3 to use termite
    sudo pacman -R xterm urxvt
    chsh -s /usr/bin/fish       # make fish your default shell

Add the following line to your `.vimrc`

    set shell=/bin/bash     " so that most scripts work

Now you can do some easy configuration

    ~/.config/termite/config
    ------------------------
    [options]
    font = Inconsolata 12
    # another font I like is Monaco
    # `fc-list` will list all installed fonts

    ~/.i3/config
    ------------
    focus_follows_mouse no  # i don't like to follow mouse for focus

To set PATH in fish, just type

    set -U file_user_paths {your_path}

This actually permanently set your PATH.

Fish's configuration file is `~/.config/fish/config.fish`.

Now, feeling excited, you can check out [fish's tutorial](http://fishshell.com/docs/current/tutorial.html) and [termite's cheatsheet](https://github.com/thestinger/termite).

Last words, now you know two shells, so next time remember to use shebang line in your script.

# How configuration works in old world (Not recommended)

In the old world, there is a file ~/.Xresources for centralized configuration. You can specify fonts, colors for terminals there. However to make that effective, you need to put the following in your ~/.xinitrc
    xrdb ~/.Xresources      # load configuration into X resource database
