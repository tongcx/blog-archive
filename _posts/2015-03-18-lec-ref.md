---
layout: post
title:  "Weapons in Linux"
tags:
- linux
---

Update:

- SSH

Today I talked about some useful tools to improve your productivity with computer. Here is a summary of everything.

- Comments are welcome
- You can also ask questions in chat room

# [vimium](https://vimium.github.io/)

- A chrome/firefox plugin let you navigate within browser really quick.
- No need to install Linux for this, just add the plugin to your web browser.
- Click the link and checkout their video and Keyboard shortcuts.

# [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

- VirtualBox is the container for virtual machines, you can install Ubuntu or any systems inside. It's a sandbox so whatever you do inside won't destroy your computer.
- Download VirtualBox by click the link. Installation is very easy.
- [Quick Start](http://lifehacker.com/5204434/the-beginners-guide-to-creating-virtual-machines-with-virtualbox)

# Ubuntu inside VirtualBox

- Install Ubuntu
    - Download [Ubuntu](http://www.ubuntu.com/download/desktop), use Ubuntu 14.10, not 14.04.2 LTS
    - In your VirtualBox create a new machine and choose Ubuntu as distribution and default options for everything else.
    - Start the machine and choose your downloaded Ubuntu image to load into virtual CD driver.
    - Install Ubuntu, really easy. For disk partition, choose the default one, it won't destroy your own computer.
    - After the reboot, you end up with a working Ubuntu.
- Some other [tutorial](http://www.psychocats.net/ubuntu/virtualbox) on this
- Fix resolution problem
    - However the resolution is bad.
    - You need to [fix it](http://askubuntu.com/questions/451805/screen-resolution-problem-with-ubuntu-14-04-and-virtualbox). You need to open a terminal to be able to enter those command to install necessary packages.
    - Click the first icon on the left to open the launcher, then type "terminal".
    - After that type `xrandr -s 1024x768` inside terminal
    - Your resolution is good now.

# Ubuntu with Windows

- Put the Ubuntu image [into a USB](http://www.ubuntu.com/download/desktop/create-a-usb-stick-on-windows)
- Then boot with the USB and then follow instruction to install it with dual boot mode
- At the end you can choose to go to Ubuntu or Window when booting
- [Official guide](https://help.ubuntu.com/community/Installation/FromUSBStick)

# [i3](https://i3wm.org/)

TODO: how to install and set up i3

- Tiling window manager that allow you to open 20 youtube at the same time
- You need Linux to use i3
- Checkout their [installation guide](https://i3wm.org/downloads/) for different Linux distributions.
- Checkout their [User guide](https://i3wm.org/docs/userguide.html).
- [reference card for keyboard shortcut](http://i3wm.org/docs/refcard.htm)

# Online chat room IRC

- Using web browser
    - [KiwiIRC](https://kiwiirc.com/client/irc.freenode.net/)
    - [Freenode web version](http://webchat.freenode.net/)

- Use weechat
    - You need Linux for this
    - Install weechat: `sudo apt-get install weechat` with Ubuntu
    - [Quick Start](https://weechat.org/files/doc/devel/weechat_quickstart.en.html)

- [How to register for a nickname on Freenode](https://plone.org/support/chat/how-to-register)


# connect to office computer

You can use [SSH](http://code.tutsplus.com/tutorials/ssh-what-and-how--net-25138) to connect to your office computer. However, usually the connection is blocked by school firewall so you need a VPN. You need to do several things

- Ask your department IT to add you to the VPN whitelist and ask them how to use it
- Install the [software](http://www.it.cornell.edu/services/vpn/) on your personal computer
- [Configure SSH server](https://www.digitalocean.com/community/tutorials/ssh-essentials-working-with-ssh-servers-clients-and-keys) on your office computer
- Now you can enjoy your office computer. If you experience difficulty, please ask questions.

# tmux

- tmus is i3 for terminal. Checkout this [tutorial](http://code.tutsplus.com/tutorials/intro-to-tmux--net-33889). Another very interesting [tutorial](https://danielmiessler.com/study/tmux/).
- one nice thing is that you won't lose your work if you got disconnected

# vim + git

- [fugitive](https://github.com/tpope/vim-fugitive) is a very nice git plugin for vim
- commit, push, diff without exiting vim

# Remote Desktop

- I'm still exploring on this, more to come
- right now I'm looking at xpra or xrdp
