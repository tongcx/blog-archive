---
layout: post
title:  "Comparing to others"
tags:
- philosophy
---

We compare to others all the time, we compare our wealth, appearance, knowledge, manner. So how is this comparison affecting us? Here is a very short list of related thoughts.

- Comparison leads to superior feeling when you're winning and annoying feeling when you're losing. But interesting thing is that for aspect you're superior, there are always more superior people to whom you may feel like a loser.
- You only observe a tiny fraction of other people's life. Other people may be sad inside when smiling or vice versa. How to compare with incomplete or even misleading data?
- People have different life objectives. How to compare your wealth with his skills even assuming complete and truthful data?
- If you view your life as a journey or movie, then all different kinds of people are just enriching your life. If everyone serves some role in your life, then what's the point of even doing this comparison?
