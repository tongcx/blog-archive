---
layout: post
title:  "Some cheat sheet"
tags:
- linux
---

# tmux
`C-a D`     | detach other

# Weechat

`C-x`     | switch between servers
/me STUFF | display event about yourself

# Vim

`C-^`              | open alternate file
`w | make! test`   | two commands on one line

# netrw@vim

`p`     | preview
`C-w z` | close preview

# git

`git remote prune origin`              | remove stale remote branches
`git branch -vv`                       | show tracking branch
`git log --graph --oneline --decorate` | beautiful git-log
`git log graph_dev ^master`            | list commits reached from graph_dev but not master
`git merge-base master dev`            | merge base of two branches
`git log master dev ^[merge-base]~1`   | diff of two commits' from their merge base
`git cat-file -p HEAD^{tree}`          | inspect the tree object of current HEAD
`git lost-found`                       | find unreachable commits
`git reflog`                           | the history of HEAD refs
`git log -p file`                      | show changes of a file across commits
`git log --oneline --graph`            | show graph structure
`git rebase -i -p`                     | rebase -i with merges perserved

# others

`systemctl list-dependency` | check all dependency
`fc-list`                   | list all installed fonts
