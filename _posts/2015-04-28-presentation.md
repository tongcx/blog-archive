---
layout: post
title:  "Presentation: presenter's view and audience's view"
tags:
- teaching
---

We saw lots of presentations. In academia world, we have seminars. In company world, we have project demos. Most of them can be improved if we think in others' shoes. I don't see how can I separate presenting and listening, so I will discuss both. Essentially, presentation is a social experience involving both parties.

# Assumptions

Hope the following assumptions are plausible and can provide a foundation for discussion.

- Motivation is very important
- Most people can only focus for 30min
- Human communication channel is of limited bandwidth

# For presenter

No matter how smart you are, if your audience takes nothing home, I think it's a failure. The following are some questions you can think about. You can first read them all and then come back to think on those interests you.

- Your audience wants to understand something which makes sense. How do you motivate them?
- If you talk abstract things, people may have no intuition. Good example can help them draw their everyday intuition.
- You probably will be bored if there are thousands of words or equations. Why that doesn't make sense? When to use picture, when to use words, equations?
- Is rigorousness the first priority in presentation? What should we delivery within 30min through limited channel?
- You don't want to lose your audience. So how about give them some chance to rest during talk?
- Will audience like you more if you tries to recite the whole thing or just having some talking points? Which one is more adaptive to your audience?
- How do you respect or care for your audience, both in presentation and Q/A?
- How is your body language affecting people's willingness to ask for clarification?
- In Q/A, how about collecting several questions and answer selectively? If you don't know the answer/don't want to answer, should you even tries to answer?

Here is some thoughts

- Give overview and conclusion. This helps audience know what's ahead and what were covered.
- You don't need to be super charismatic. Just be yourself. Even Elon Musk laughs like a child when introducing Powerwall.

# For audience

We cannot demand all presenters to be perfect. What can you do to help yourself and others?

- If there isn't enough concrete example, can you try to think about one? If not, is it a good idea to ask presenter for one?
- Do you want to pay full attention all the time? Is it okay to play with your cellphone or just think about other things?
- If the presenters are bombarding you with tons of information, how could you navigate through it?
- If you have a question which will break the presentation flow, when to ask?
- Do you want to get all details solid sitting in presentation?
- If you enjoy the talk, what can you do to encourage presenter, making him/her feel like it's worth it?
