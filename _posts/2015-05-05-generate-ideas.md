---
layout: post
title:  "Subconscious and ideas"
tags:
- tech
---

Sometimes, when stuck with seemly impossible problems, we crave for idea generator, an machine spill out ideas. How wonderful? Unfortunately, we have to rely on human brains. So how does our brains generate ideas and how to facilitate that?

Before going forward, ask yourself a question: are you really interested in the problem you're trying to solve? Does your problem live in some inspiring context? If not, can you find one?

# Conscious and Subconscious

I think conscious and subconscious of two different natures.

**Conscious**

- Sequential computation, feels like finding a path from A to B with map.
- Need to focus, could be tiring, hard to handle multiple tasks
- Suitable for following directions
- High priority, you have to hold it or ditch it

**Subconscious**

- Parallel computation, generating high level and qualitative summaries
- When you're enjoying a painting, your pictorial thinking is mostly subconscious
- Draw massive amount of information
- Don't need to focus, less tiring, can handle multiple tasks
- Low priority, it can wait, it can run in background

**Their connection**

- Once your subconscious finds something interesting, it will pop up to your conscious

# Use cases for Conscious and Subconscious

I think subconscious is for "Where to go?", conscious is for "How to go there?". And ideas usually are actually "Where to go?".

**Conscious**

- Formalize your ideas
- Coding
- Cooking steak

**Subconscious**

- Chatting with your friends/family
- Invent new dishes

# How to generate ideas? A.K.A How to keep subconscious active?

- High pressure environment makes your conscious strong and subconscious weak. Avoid it.
- Hangout with friends/family. Engage in workout. Sleep well. Make your subconscious healthy.
- Go to seminar, discuss with other people. By far, this is the most useful one for me. The reason is that it keeps your immediate attention away from the problems you're solving while still immersing yourself in relevant environment. Thus, your subconscious is working like crazy.
- Don't pay too much attention in seminar. Otherwise your conscious will drive away your subconscious. It's okay, or even desirable, to let your mind slip away at times.

# Conclusion

As we can see, subconscious is more important in generating ideas. We also discussed how to keep subconscious active. Before concluding, I'd like to say this phenomena really resonate with one philosophy: don't try too hard.

Sometimes, when you pursue something really hard, it just eludes. But once you get loose yourself, beautiful things may happen. Competition/pressure may make you highly focused, however, that may harm your creativity.
