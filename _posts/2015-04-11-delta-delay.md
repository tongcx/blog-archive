---
layout: post
title:  "How could Delta handle delay flights better?"
tags:
- business
---

On Wednesday I was taking flight from SMF to BOS. Initially, my flight should depart around 1:30pm but there were mechanical problem with the plane. I ended up changing flight departing at 5:30pm.

Lots of people were blaming Delta for their delay. I also blame Delta, but for different reasons. Bad things happen all the time, blaming Delta probably won't prevent mechanical problems from happening. (Maybe we can, supposing Delta isn't doing routine check carefully enough. Actually I experienced another mechanical problem on my changed flight. But this is not the point of this post). However we can ask whether Delta's doing its best to help customers and themselves under crisis scenario.

# False hope

From the time we learned there is a mechanical problem, they started to giving misleading time estimate. "We should be able to figure it out in 20min. Oh no, 1h. No again, 2h probably." I agree the diagnosis is an adaptive procedure that estimate is constantly updated. However when they say "20min", they really mean some initial check can be done in 20min. Even if that's enough to pin down the problem, they still need more time to fix the problem. So 20min is very inaccurate. It's a bad lower bound.

I think if they put more thought and try to make better estimate, passengers will be psychologically better off. More importantly, better estimate make people more informed, which leads to better decisions and everyone will be better off.

# Long waiting line

When people were disillusioned about the original flight, they formed a very long line to talk to agent to change flight. Maybe you have another option which will take off in 20min, but you have to wait in this slow line, watching your options slipping away. Naturally, people were fretting.

I think Delta can do much better for crisis handling.

- They can divert people to other gates with idling agents. This is a very practical option and there is almost no change or cost for Delta to do so. Actually, an agent later tell me I can go to the Delta customer service in the airport and they can also help me change my flight. This information should have been disclosed much earlier.
- They can make it easier for people to call to make change. Instead of letting people waiting in the line in critical situation, we can let people call the more urgent line to avoid waiting. I think a better and more structured way is to give people one-time access code which determines priority of this call.
- Their app can actually handle no-charge change like this. However, the options I got is not as nice as talking to agent. Hopefully this can be fixed. If this option works very well, then they should advertise it so that people can get the change done in the most agony-free way. At the same time, this will occupy less of Delta's manpower and more seats on near-departure flights.

# Mix feeling for delayed flight

What kind of customer relationship do we want in such scenario? Do we want Delta to babysit customers? Or do we want they to work together?

I argue for the later. If Delta babysits customer, customers have nothing to do beside complains. Instead, if people get the impression that airline also took a hit, then they feel they're on the same team and may work together and eventually customers probably feel better this way.

Naturally, it's quite clear why Delta takes a hit. When a flight cannot fly on time, then if people change their flight, the original flight was then under-utilized which reduce profit for Delta. Of course, there is downward chain effect causing other delays.

However, on the other hand, part of the loss can be compensated by selling seats of this flight for last-minute customers. Also, maybe some customers prefer this flight with delayed time than their original one. How knows?

It seems that airlines don't like the idea of working together when crisis happens, if United help Delta in this case, then Delta may help United in future. Large cooperation can increase resilience against unexpected events. I think the reason why they don't cooperate is probably twofold. On the political side, they are competitors. On the practical side, how do they handle customer fares and what agreement could they have is unclear to me. Also how to move checked bags to other airlines and how to handle inconsistency in flight experience pose some other concerns.

Anyway, for delayed flight, my feeling is mixed.
